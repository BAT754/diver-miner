// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PerformButtonAction(action)
{
	switch (action)
	{
		case 0:
			obj_controller.state = gameState.preparePlay;
			obj_controller.alarm[2] = 60;
		
		break;
		
		case 1:
			
			obj_controller.replaying = true;
			obj_controller.alarm[10] = 30;
		
		break;
		
		case 2:
			show_debug_message("END");
			game_end();
		
		break;
		
	}

}