// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function pickTile(tile, strength){
	
	tile.hardness -= strength;
	
	if (tile.hardness <= 0)
	{
		tile.alarm[0] = 1;
		audio_sound_pitch(snd_blockDestroyed, .5);
		audio_play_sound(snd_blockDestroyed, 4, false);
	}
	else
	{
		audio_sound_pitch(snd_blockDestroyed, 1.3)
		audio_play_sound(snd_blockDestroyed, 5, false);
	}
}