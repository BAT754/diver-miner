// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function generateFirstChunk(chunk)
{
	var leaveEmpty = irandom_range(4,7);
	
	// Add sides - these will always be the same
	for (var i = 0; i < colSize; i++)
	{
		ds_grid_set(chunk, 0, i, 10);
		ds_grid_set(chunk, rowSize - 1, i, 10);
	}
	
	// First Layer
	ds_grid_set(chunk, 1, 0, 1);
	ds_grid_set(chunk, 2, 0, 1);
	ds_grid_set(chunk, 9, 0, 1);
	ds_grid_set(chunk, 10, 0, 1);
	
	// Second Layer
	ds_grid_set(chunk, 1, 1, 1);
	ds_grid_set(chunk, 2, 1, 1);
	ds_grid_set(chunk, 3, 1, 1);
	ds_grid_set(chunk, 8, 1, 1);
	ds_grid_set(chunk, 9, 1, 1);
	ds_grid_set(chunk, 10, 1, 1);
	
	// Third Layer
	for (var i = 1; i < rowSize - 1; i++)
	{
		ds_grid_set(chunk, i, 2, 1);
	}
	
	// Forth layer. Introduce first hole
	for (var i = 1; i < rowSize - 1; i++)
	{
		if (leaveEmpty == i)
		{
			ds_grid_set(chunk, i, 3, 0);
			pathEndX = i - 1;
		}
		else
			ds_grid_set(chunk, i, 3, 1);
	}
	
	// Final layer. Start randomly deciding the path
	generateRow(chunk, 4);

}


function generateChunk (chunk)
{
	// Add sides - these will always be the same
	for (var i = 0; i < colSize; i++)
	{
		ds_grid_set(chunk, 0, i, 10);
		ds_grid_set(chunk, 11, i, 10);
	}
	
	// Generate the rows
	generateRow(chunk, 0);
	generateRow(chunk, 1);
	generateRow(chunk, 2);
	generateRow(chunk, 3);
	generateRow(chunk, 4);
}

function loadNewChunk (chunk, xStart, yStart)
{
	for (var i = 0; i < colSize; i++)
	{
		for (var j = 0; j < rowSize; j++)
		{
			var type = ds_grid_get(chunk, j, i);
			
			if (type == 0)
			{
				var block = instance_create_layer(xStart + (j*blockSize), yStart + (i * blockSize) - obj_controller.obstacleSpeed, "Terrain", obj_water);
			}
			else
			{
				var block = instance_create_layer(xStart + (j*blockSize), yStart + (i * blockSize) - obj_controller.obstacleSpeed, "Terrain", obj_terrain);
				with (block)
				{
					self.terrainType = type;
					fillTerrainData(self);
				}
			
				// This is the block that will trigger a new chunk being made when it reaches a certain y cord.
				if (i == colSize - 1 && j == 0)
				{
					monitorBlock = block;	
				}
			}
		}
		instance_create_layer(xStart + blockSize/2, yStart + (i * blockSize), "GUI", obj_distanceMarker);
	}
}

function fillTerrainData (block)
{
	switch (block.terrainType)
	{
		case 0:
			block.hardness = 0;
		break;
		
		case 1:
			block.sprite_index = spr_dirt;
			block.image_speed = 0;
			block.hardness = 1;
		break;
		
		case 2:
			block.sprite_index = spr_stone;
			block.image_speed = 0;
			block.hardness = 2;
		break;
		
		case 3:
			block.sprite_index = spr_coal;
			block.image_speed = 0;
			block.hardness = 3;
		break;
		
		case 10:
			block.sprite_index = spr_wall;
			block.image_speed = 0;
			block.hardness = 1000;
		break;
		
	}
}

function pickBlock ()
{
	var level = obj_controller.distance;
	var chance = irandom_range(1, 100);
	
	if (level < 15)
		return 1;
	else if (level < 45)
	{
		if (chance < 15)
			return 3; // 2
		else
			return 1; 
	}
	else if (level < 90)
	{
		if (chance < 30)
			return 2;	
		else if (chance < 50)
			return 3;
		else
			return 1;
	}
	else if (level < 130)
	{
		if (chance < 45)
			return 2;
		else
			return 1;
	}
	else if (level < 160)
	{
		if (chance <= 50)
			return 2;
		else if (chance < 98)
			return 1;
		else 
			return 10;
	}
	else
	{
		if (chance < 46)
			return 2;
		else if (chance <  91)
			return 1;
		else 
			return 10;
	}
}

function swapChunks(chunk, old)
{
	var num = 0;
	for (var i = 0; i < colSize; i++)
	{
		for (var j = 0; j < rowSize; j++)
		{
			num = ds_grid_get(chunk, j, i);
			ds_grid_set(old, j, i, num);
		}
	}
	
	makingChunk = false;
}

function generateRow(chunk, rowFilling)
{
	var routeNum = 0;
	var route = 0;
	var length = 0;
	var row = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
	
	routeNum = irandom_range(1, 100);
	
	// Generate a path type
	if (pathEndX == 0)
	{
		if (routeNum <= 50)
			route = pathState.goDown;
		else if (routeNum <= 95)
			route = pathState.goRight;
		else
			route = pathState.stop;
	}
	else if (pathEndX == 9)
	{
		if (routeNum <= 50)
			route = pathState.goDown;
		else if (routeNum <= 95)
			route = pathState.goLeft;
		else 
			route = pathState.stop;
	}
	else if (pathEndX == -1)
	{
		pathEndX = irandom_range(0, 9);
		route = pathState.goDown;
	}
	else
	{
		if (routeNum <= 35)
			route = pathState.goDown;
		else if (routeNum <= 65)
			route = pathState.goRight;
		else if (routeNum <= 95)
			route = pathState.goLeft;
		else 
			route = pathState.stop;
		
	}
	
	// Generate the row based on the path type chosen
	switch (route)
	{
		case pathState.goDown:
			// Put an empty block below the empty block
			row[pathEndX] = 0;
			
			// Fill in the rest
			for (var i = 0; i < 10; i++)
			{
				if (row[i] == -1)
				{
					// Fill with block
					var picked = pickBlock();
					row[i] = picked;
				}
			}
			pathEndX = pathEndX;
			
		break;
		
		case pathState.goRight:
		
			length = irandom_range(1, (9 - pathEndX));
			
			// Put in the empty blocks
			for (var i = 0; i <= length; i++)
			{
				row[pathEndX + i] = 0;	
			}
			pathEndX = pathEndX + length;
			
			// Put in the rest
			for (var i = 0; i < 10; i++)
			{
				if (row[i] == -1)
				{
					var picked = pickBlock();
					row[i] = picked;
				}
			}
		
		break;
		
		case pathState.goLeft:
		
			length = irandom_range(1, pathEndX);
			
			// Put in the empty blocks
			for (var i = 0; i <= length; i++)
			{
				row[pathEndX - i] = 0;	
			}
			pathEndX = pathEndX - length;
			
			// Put in the rest
			for (var i = 0; i < 10; i++)
			{
				if (row[i] == -1)
				{
					var picked = pickBlock();
					row[i] = picked;
				}
			}
		
		break;
		
		case pathState.stop:
		
			pathEndX = -1;
			
			for (var i = 0; i < 10; i++)
			{
				var picked = pickBlock();
				row[i] = picked;	
			}
		
		break;
	} // End switch
	
	// Put that row in the cunks row
	for (var i = 0; i < 10; i++)
	{
		ds_grid_set(chunk, i+1, rowFilling, row[i]);	
	}
	
		
}