/** Brain Storming - Delete later probably

Distance Diver - Pretty standard "how long can you last" type game.
	- Most feasable
	
	- Top to Bottom, going deep underwater
		- Deeper depths reveal different gems
		- Upgade materials with gems found while delving
			- Pick axe
			- Armor
			- Scuba tank / rage (stubborness)
			
	I'm doing this one
	
	
	Major game Aspects:
		- Distance counter / Highscore tracker
		- Generate terrain/dungeon
			- will it be levels, or infinite runner?
			- Different terrain types
		- Resource Gathering
			- Inventory limits? Or just one big store
			- How many types of resources?
			- Spawning rates
		- Upgradeable materials
			- Will appearance change?
			- Gotta keep cost/upgrade benifit balanced.
		- Gotta look good
			- How to get underwater feel? Shader maybe? Or just a blue rectangle with low opacity.
			- Don't go too crazy though. You're not a pixel artist. You'll figure it out.
		- Obstacles?
			- Bubbles - raise you up, but can also give you are
			- Attackers? 
			
			
		- Edward Bellamy - EB for short
			
		- ALright. Let's do this.


*/