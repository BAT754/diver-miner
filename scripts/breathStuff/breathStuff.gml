// Collection of functions for monitoring and altering the breath bar.

// Restore breath to the player
function gainBreath(airBar, amount)
{
	var breath = airBar.currBreath;
	var newBreath = breath + amount;
	
	if (newBreath >= airBar.maxBreath)
	{
		newBreath = airBar.maxBreath;	
	}
	
	airBar.currBreath = newBreath;
}