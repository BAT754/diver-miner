{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 6,
  "bbox_right": 120,
  "bbox_top": 9,
  "bbox_bottom": 124,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e3e764eb-3165-4fef-906b-17003df587ec","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3e764eb-3165-4fef-906b-17003df587ec","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"37207f5b-bb38-41d0-af28-aba5ae207f16","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e3e764eb-3165-4fef-906b-17003df587ec","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"b62fd9ca-ca5b-4517-8ce9-c6aa26663eb5","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e3e764eb-3165-4fef-906b-17003df587ec","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"e35a164c-f1c7-4867-8c91-fd147663f805","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e3e764eb-3165-4fef-906b-17003df587ec","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"d17001b5-f727-49b6-9f90-7bc7802c16c9","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e3e764eb-3165-4fef-906b-17003df587ec","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"fa8d2ede-b684-4fe9-83ac-dae85364f630","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tutorial","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"e3e764eb-3165-4fef-906b-17003df587ec","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"d17001b5-f727-49b6-9f90-7bc7802c16c9","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"fa8d2ede-b684-4fe9-83ac-dae85364f630","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"e35a164c-f1c7-4867-8c91-fd147663f805","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"b62fd9ca-ca5b-4517-8ce9-c6aa26663eb5","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","path":"sprites/spr_tutorial/spr_tutorial.yy",},"LayerId":{"name":"37207f5b-bb38-41d0-af28-aba5ae207f16","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tutorial","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_tutorial","path":"sprites/spr_tutorial/spr_tutorial.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"dcd703f1-16e2-46c6-b7ee-938ef384416e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3e764eb-3165-4fef-906b-17003df587ec","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0be1bb58-1606-4471-beeb-6e654cbc62d7","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b92486a-3bf3-43ae-b5ad-35e601b3b6b6","path":"sprites/spr_tutorial/spr_tutorial.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_tutorial","path":"sprites/spr_tutorial/spr_tutorial.yy",},
    "resourceVersion": "1.3",
    "name": "spr_tutorial",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 3","resourceVersion":"1.0","name":"d17001b5-f727-49b6-9f90-7bc7802c16c9","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 4","resourceVersion":"1.0","name":"fa8d2ede-b684-4fe9-83ac-dae85364f630","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"e35a164c-f1c7-4867-8c91-fd147663f805","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"b62fd9ca-ca5b-4517-8ce9-c6aa26663eb5","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"37207f5b-bb38-41d0-af28-aba5ae207f16","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_tutorial",
  "tags": [],
  "resourceType": "GMSprite",
}