{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 95,
  "bbox_top": 0,
  "bbox_bottom": 95,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 96,
  "height": 96,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5cca6afc-103e-4593-a25a-4bf055afd627","path":"sprites/spr_coal/spr_coal.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5cca6afc-103e-4593-a25a-4bf055afd627","path":"sprites/spr_coal/spr_coal.yy",},"LayerId":{"name":"c03fe2af-1171-41c2-9546-454b41514927","path":"sprites/spr_coal/spr_coal.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"5cca6afc-103e-4593-a25a-4bf055afd627","path":"sprites/spr_coal/spr_coal.yy",},"LayerId":{"name":"d9a23ee4-1bd3-4ef5-b62b-407855ae6aa2","path":"sprites/spr_coal/spr_coal.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"5cca6afc-103e-4593-a25a-4bf055afd627","path":"sprites/spr_coal/spr_coal.yy",},"LayerId":{"name":"52c92de3-2764-4791-9519-ca4e146a3164","path":"sprites/spr_coal/spr_coal.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_coal","path":"sprites/spr_coal/spr_coal.yy",},"resourceVersion":"1.0","name":"5cca6afc-103e-4593-a25a-4bf055afd627","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_coal","path":"sprites/spr_coal/spr_coal.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ef155926-b53c-4ae6-be60-b6f67c02260a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5cca6afc-103e-4593-a25a-4bf055afd627","path":"sprites/spr_coal/spr_coal.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_coal","path":"sprites/spr_coal/spr_coal.yy",},
    "resourceVersion": "1.3",
    "name": "spr_coal",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1 (2)","resourceVersion":"1.0","name":"52c92de3-2764-4791-9519-ca4e146a3164","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Terrain Types",
    "path": "folders/Sprites/Terrain Types.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_coal",
  "tags": [],
  "resourceType": "GMSprite",
}