if (mouse_check_button_pressed(mb_left))
{
	
	if (!rotating && canPick)
	{
		clicking = instance_place(mouse_x + pickOffset, mouse_y + pickOffset, obj_terrain);
		
		if (clicking != noone)
		{
			// Start mining aniamtion
			rotating = true;
		}
	}
}

if (rotating)
{
	x += ((clicking.x + 64) - x) / moveSpeed;
	y += ((clicking.y + 64) - y) / moveSpeed;
	rotatAmount = animcurve_channel_evaluate(channel, animPoint);
	animPoint += animSpeed;
	image_angle = rotatAmount;
	chaseMouse = true;
	
	if (animPoint >= 1)
	{
		rotating = false;
		alarm[0] = 1;
	}
}
else
{
	if (chaseMouse)
	{
	
		x += ((mouse_x + pickOffset) - x) / moveSpeed;
		y += ((mouse_y + pickOffset) - y) / moveSpeed;
		
		if (point_distance(x, y, mouse_x + pickOffset, mouse_y + pickOffset) < 7)
			chaseMouse = false;
	}
	else
	{
		x = mouse_x + pickOffset;
		y = mouse_y + 20;
	}
	
	
}