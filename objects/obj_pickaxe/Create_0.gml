window_set_cursor(cr_none);


curveAsset = anim_cursor_rotation;
curveStruct = animcurve_get(curveAsset);
channel = animcurve_get_channel(curveStruct, 0);

rotatAmount = 0;
animPoint = 0;
animSpeed = 0.02;
rotating = false;
doneRotating = false;
resetRotation = image_angle;
chaseMouse = false;
canPick = false;
pickOffset = 17;

// Gameplay Variables
pickStrength = 1;
clicking = noone;
moveSpeed = 7;

