if (rotating || chaseMouse)
{
	draw_set_alpha(0.7);
	draw_sprite(spr_cursor_shadow, 0, mouse_x + pickOffset, mouse_y + pickOffset);
}

draw_set_alpha(1);

draw_self();


/// SHOWS THE X AND Y OF THE PLAYER OBJECT
/*
if (instance_exists(obj_dwarf))
{
	draw_set_color(c_white);
	draw_rectangle(10, 10, 250, 32, false);
	
	draw_set_font(fnt_newHS);
	draw_set_valign(fa_top);
	draw_set_halign(fa_left);
	draw_set_color(c_black);
	draw_text(15, 13, "X: " + string(obj_dwarf.x) + "  Y: " + string(obj_dwarf.y));
}
*/