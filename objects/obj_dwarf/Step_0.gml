/// @desc do all this stuff

// Movement Code
if (canMove)
{
	if (keyboard_check(ord("A")))
	{
		horizMove = -horizSpeed;
		facingRight = false;
	}
	else if (keyboard_check(ord("D")))
	{
		horizMove = horizSpeed;
		facingRight = true;
	}
	else	
	{
		horizMove = 0;
	}

	if (keyboard_check_pressed(ord("W")))
		jump = true;
	else 
		jump = false;
}
	

	
// No jumping unless on surface?
	// This means swimming up isn't possible
		// But they're a big chonky dwarf, so that makes sense, right?
if (jump && onSurface)
{
	if (!place_meeting(x, y - jumpSpeed, obj_terrain))
	{
		onSurface = false;
		fallSpeed -= jumpSpeed;
	}
}
	
// Gravity - It's gotta feel kinda relaxed, since underwater. 
if (!onSurface)
{
	if (fallSpeed <= gravSpeed)
		fallSpeed += gravRate;
}

// Checks for vertical collisions
if (place_meeting(x, y + fallSpeed, obj_terrain))
{
	while (!(place_meeting(x, y + sign(fallSpeed), obj_terrain)))
	{
		y += sign(fallSpeed);	
	}
	fallSpeed = 0;	
}

// Move the world around the player
if (y < room_height - fallBuffer)
{
	y += fallSpeed;
	
}
else
{
	y = room_height - fallBuffer;
	obj_controller.obstacleSpeed = fallSpeed;
}

// Checks for horizontal collisions
if (place_meeting(x + horizMove, y, obj_terrain))
{
	while (!(place_meeting(x + sign(horizMove) + (1 * sign(horizMove)), y, obj_terrain)))
	{
		x += sign(horizMove);
	}
	horizMove = 0;
}
x += horizMove;

// Decides if we are on top of a platform or not
if (place_meeting(x, y + 1, obj_terrain))
{
	onSurface = true;
	fallSpeed = 0;
	
	touching = instance_place(x, y+1, obj_terrain);
	//fallSpeed = -(touching.moveSpeed);
	
	// Trying to see if this stops the yeeting to the top of the screen
	//if (y + sprite_height - touching.moveSpeed > touching.y)
	//{
	y = touching.y - sprite_height;
	//}
}
else
{
	onSurface = false;
	touching = noone;
}

// breath holding
if (underwater && !outOfBreath)
{
	if (!dockBreath)
	{
		dockBreath = true;
		alarm[0] = 30;
	}
}






// Animate
if (horizMove == 0)
{
	//sprite_index = spr_dwarf_standing;
}
else
{
	if (facingRight)
	{
		//sprite_index = spr_dwarf_moving;	
	}
	else
	{
		//sprite_index = spr_dwarf_moving_left;
	}
}









