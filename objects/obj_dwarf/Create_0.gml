/// @desc Dwarf's starting variables


// Movement variables ---------------------------- //
horizSpeed = 6;
horizMove = 0;

// Used when jumping off a surface
jumpSpeed = 12;

gravSpeed = 8;
gravRate = 1;
fallSpeed = 0;

maxBreath = 100;
underwater = false;
dockBreath = false;
outOfBreath = false;

jump = false;
facingRight = true;
fallBuffer = 200;

onSurface = false;
touching = noone;
canMove = false;

// Other stuff ---------------------------- //
image_speed = 1;

breathBar = instance_create_layer(30, 20, "GUI", obj_breathBar);
breathBar.player = self;
breathBar.maxBreath = self.maxBreath;
breathBar.currBreath = self.maxBreath;
