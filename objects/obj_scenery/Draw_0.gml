/// @desc Draw the pretty stuff

if (drawTopScenery)
{
	
	// Clouds
	draw_sprite(spr_clouds, 0, 100, 100 + y);
	draw_sprite(spr_clouds, 1, 400, 70 + y);
	draw_sprite(spr_clouds, 2, 600, 175 + y);
	
	// The Forge
	draw_sprite(spr_forge, 0, 330, 450 + y);
	
	// Waves at bank of pool thing
	draw_sprite(spr_waves, 0, 448, 576 + y);
	
	draw_sprite(spr_title, 0, room_width/2, titleY + y);
	
}

	// Draw Water
draw_sprite(spr_water, 0, 0, waterY);

if (drawTopScenery)
{
	
	// Grassy Banks
	draw_sprite(spr_leftBank, 0, 0, 448 + y);
	draw_sprite(spr_rightBank, 0, 1023, 448 + y);
}

// Tutorial Pictures
draw_set_alpha(0.7);
draw_sprite(spr_tutorial, 0, 250, 700 + y);
draw_sprite(spr_tutorial, 1, 700, 700 + y);
draw_set_alpha(1);
