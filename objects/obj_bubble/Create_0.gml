/// @description bubble values


bubbleSpeed = 2; // How fast the bubble moves
airAmount = 20; // How much air the bubble can hold
moveSpeed = 0; // used for moving the bubble when the world is moving
bubbleLife = 300; // how long the bubble lasts after being made

// Stuff to help the bubble wiggle
curveAsset = anim_bubbleWiggle;
curveStruct = animcurve_get(curveAsset);
channel = animcurve_get_channel(curveStruct, 0);
animPoint = 0;
animSpeed = 0.005;
wiggleAmount = 0;

raising = true;

// Start the bubble's count down to poppage
alarm[0] = bubbleLife;
