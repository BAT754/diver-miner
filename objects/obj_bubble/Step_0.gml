/// @description Insert description here
// You can write your code in this editor

// Sets the speed the bubble might move at (the bubble's normal speed + scroll Speed)
moveSpeed = bubbleSpeed + obj_controller.obstacleSpeed;

// Checks to see if the bubble hits a dirt block above it
if (place_meeting(x, y - moveSpeed, obj_terrain))
{
	while (!(place_meeting(x, y - 1, obj_terrain)))
	{
		y -= 1;	
	}
	moveSpeed = obj_controller.obstacleSpeed;
	raising = false;
}
else
{
	raising = true;
}

// Raise the bubble upwards
y -= moveSpeed;

// Give the bubble some wiggle
wiggleAmount = animcurve_channel_evaluate(channel, animPoint);
animPoint += animSpeed;

if (animPoint >= 1)
{
	animPoint = 0;
}

// Check for side collisions while wiggling
if (place_meeting(x + wiggleAmount, y, obj_terrain))
{
	while (!(place_meeting(x + sign(wiggleAmount), y, obj_terrain)))
	{
		x += sign(wiggleAmount);	
	}
	wiggleAmount = 0;
}

if (raising)
	x += wiggleAmount;
else
	x += wiggleAmount / 4;


// Check to see if the bubble has left the screen
if (y < -sprite_height + 10)
{
	instance_destroy(self);
}


