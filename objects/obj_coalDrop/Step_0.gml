/// @description Insert description here
// You can write your code in this editor

// Since these guys will fall, their overall moveSpeed should be their fallSpeed - scroll Speed
moveSpeed = fallSpeed - obj_controller.obstacleSpeed;

// Keeps the object from clipping into the other object
if (place_meeting(x, y + moveSpeed, obj_terrain))
{
	while (!(place_meeting(x, y + 1, obj_terrain)))
	{
		y += 1;	
	}
	
	moveSpeed = -obj_controller.obstacleSpeed;
}

y += moveSpeed;

while (place_meeting(x, y, obj_terrain))
{
	y -= 1;	
}

// Check to see if the coal has left the screen
if (y < -sprite_height + 10 || y > 1000)
{
	instance_destroy(self);	
}