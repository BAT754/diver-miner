other.jumpSpeed = 14;
other.gravSpeed = 5;
other.gravRate = 0.45;

if (other.fallSpeed > other.gravSpeed)
{
	other.fallSpeed = other.gravSpeed;
	other.underwater = true;
}

//obj_controller.obstacleSpeed = 2;
obj_songManager.passedLine = true;
instance_destroy();