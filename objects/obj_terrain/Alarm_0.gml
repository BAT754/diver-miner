/// @descr DESTROY THE THING

if (terrainType == 1)
{
	var dropOxygen = irandom_range(1, 100);
	if (dropOxygen <= bubbleChance)
	{
		var variation = irandom_range(-20, 20);
		instance_create_layer(x + (sprite_width/4) + variation, y + (sprite_height/2), "Terrain", obj_bubble);
	}
}
else if (terrainType == 3)
{
	var numDropped = irandom_range(1, 4);
	
	for (var num = 0; num < numDropped; num++)
	{
		var variation = irandom_range(-20, 20);
		instance_create_layer(x + sprite_width/3 + variation, y + sprite_height/4, "Terrain", obj_coalDrop);	
	}
}


instance_destroy();