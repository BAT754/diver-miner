// Path Creation Enum
enum pathState
{
	goDown,
	goLeft,
	goRight,
	stop
}

// Links to the game's main controller
controller = noone;

// Grid size variables
rowSize = 12;
colSize = 5;
blockSize = 96;

// Gameplay variables
lastChunk = ds_grid_create(rowSize, colSize);
newChunk = ds_grid_create(rowSize, colSize);
monitorBlock = noone;
makingChunk = false;

spawnX = -64;
spawnY = 928;

pathEndX = 0;
pathEndY = 0;

generateFirstChunk(lastChunk);
loadNewChunk(lastChunk, spawnX, spawnY);
