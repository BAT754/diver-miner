// Spawn the sides of rocks

if (monitorBlock.y < 832 && !makingChunk)
{
	makingChunk = true;
	generateChunk(newChunk);
	loadNewChunk(newChunk, monitorBlock.x, monitorBlock.y + blockSize);
	swapChunks(newChunk, lastChunk);
}