/// @desc show buttons

if (!replayButton.showMe)
{	
	replayButton.x = 250;
	replayButton.y = 400;
	replayButton.showMe = true;
	alarm[5] = 30;
}
else
{
	quitButton.x = 550;
	quitButton.y = 400;
	quitButton.showMe = true;
	
	if (distance > obj_scoreKeeper.highscore)
	{
		obj_scoreKeeper.highscore = distance;
		newHighscore = true;
	}
}
