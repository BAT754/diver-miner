/// @desc This guy controlls the game

enum gameState
{
	prepareMenu,
	menu,
	preparePlay,
	play,
	gameOver
}

randomize();

// Gameplay variables
obstacleSpeed = 0;
distance = 0;
state = gameState.prepareMenu;
speedUpInterval = 15;
speedUpRate = 0.2;
replaying = false;
newHighscore = false;

gameOverTextY = -200;

generator = instance_create_layer(0,0, "GUI", obj_terrainGenerator);
generator.controller = self;

// Buttons
playButton = instance_create_layer(704, 128, "Buttons", obj_button);
with (playButton)
{
	sprite_index = spr_playButton;
	action = 0;
}

replayButton = instance_create_layer(750, 200, "Buttons", obj_button);
with (replayButton)
{
	sprite_index = spr_replayButton;
	action = 1;
	showMe = false;
}

quitButton = instance_create_layer(650, 200, "Buttons", obj_button);
with (quitButton)
{
	sprite_index = spr_quitButton;
	action = 2;
	showMe = false;
}

player = instance_create_layer(352, 320, "Character", obj_dwarf);


// Animation Variables
fadeAlpha = 1;
fadeDone = false;
scenery = instance_create_layer(0, 0, "Scenery", obj_scenery);

// Music
musicMan = instance_create_layer(0, 0, "GUI", obj_songManager)