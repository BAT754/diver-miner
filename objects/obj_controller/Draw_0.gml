switch (state)
{
	case gameState.prepareMenu:
	
		draw_set_alpha(fadeAlpha);
		draw_set_color(c_black);
		draw_rectangle(-1, -1, room_width + 1, room_height + 1, false);
		
		draw_set_alpha(1);
		
	
	break;
	
	
	case gameState.menu:
		draw_set_alpha(1);
	
	break;
	
	case gameState.play:
	
		// Distance Tracker
		draw_sprite(spr_distanceTracker, 0, 700, 60);
		
		draw_set_font(fnt_score);
		draw_set_color(c_black);
		draw_set_halign(fa_right);
		draw_set_valign(fa_middle);
		draw_text(930, 120, string(distance) + " m");
	
	break;
	
	case gameState.gameOver:
		
		// Distance Tracker
		if (fadeAlpha < .8)
		{
			draw_set_alpha(1 - fadeAlpha)
			{
				draw_sprite(spr_distanceTracker, 0, 700, 60);
		
				draw_set_font(fnt_score);
				draw_set_color(c_black);
				draw_set_halign(fa_right);
				draw_set_valign(fa_middle);
				draw_text(930, 120, string(distance) + " m");
			}
		}
	
		draw_set_alpha(fadeAlpha);
		draw_set_color(c_black);
		draw_rectangle(-1, -1, room_width + 1, room_height + 1, false);
		
		draw_set_alpha(1);
		
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_set_font(fnt_gameOver);
		draw_set_color(c_white);
		draw_text(room_width/2, gameOverTextY, "Game Over");
		
		if (newHighscore)
		{
			draw_set_font(fnt_newHS);
			draw_text(room_width/2, gameOverTextY + 50, "New Highscore: " + string(obj_scoreKeeper.highscore) + " m!");
		}
	
	break;
	
}






/*
if (obj_dwarf.onSurface)
	draw_text(20, 100, "ON SURFACE");
else
	draw_text(20, 100, "FALLING.");
	
draw_text(600, 40, "Distance: " + string(distance) + " m");

draw_text(600, 60, "Instances: " + string(instance_count));
draw_text(600, 80, "Making Chunk: " + string(obj_terrainGenerator.makingChunk));
draw_text(600, 100, "EndX: " + string(obj_terrainGenerator.pathEndX));
draw_text(600, 120, "Speed: " + player.textToShow);
*/