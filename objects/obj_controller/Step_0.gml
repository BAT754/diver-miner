switch (state)
{
	case gameState.prepareMenu:
	
	
	
	if (fadeAlpha > 0)
		fadeAlpha -= 0.02;
	else if (!fadeDone)
	{
		fadeDone = true;
		fadeAlpha = 0;
		alarm[1] = 30;
	}
	
	break;
	
	
	case gameState.menu:
		
	
	
	
	break;
	
	case gameState.preparePlay:
	
	
	break;
	
	case gameState.play:
		// Speeding up is handled in the distanceMarker object
		
		if (player.outOfBreath)
		{
			state = gameState.gameOver;
			alarm[4] = 1;
		}
		
	break;
	
	case gameState.gameOver:
		
		if (fadeAlpha < .8 || replaying)
			fadeAlpha += 0.02;
		else
		{
			fadeAlpha = 0.8;
			fadeDone = true;
		}
		
		if (fadeDone && gameOverTextY < 300)
		{
			gameOverTextY += 5;
			alarm[5] = 10;
		}
		
	break;
	
}

if (keyboard_check_pressed(ord("R")))
	game_restart();
