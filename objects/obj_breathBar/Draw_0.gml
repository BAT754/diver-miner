// Draw background
draw_set_alpha(.75);
draw_sprite(spr_breathBarBackground, 0, x, y);

// Draw status bar
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_alpha(1);
draw_set_color(c_blue);
draw_rectangle(statusX, statusY, statusX + (300 * barSize), statusY + 40, false);

// Draw over layer
draw_set_color(c_white);
draw_sprite(spr_breathBar, 0, x, y);

// Draw the amounts
draw_set_color(c_white);
draw_set_font(fnt_breath);
draw_text(x + 140, y+50, string(ceil(100 * barSize)) + "%");

